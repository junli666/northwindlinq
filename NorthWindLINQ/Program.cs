﻿using System;

namespace NorthWindLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities nwe = new NorthwindEntities();
            ProductsQuery pq = new ProductsQuery(nwe);

            Console.WriteLine("-------------getTopProduct with country code----------------------------");
            pq.getTopProduct("UK");

            Console.WriteLine("-------------getTopProduct ----------------------------");
            pq.getTopProduct();



        }
    }
}
