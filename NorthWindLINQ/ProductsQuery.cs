﻿using System.Linq;

namespace NorthWindLINQ
{
    class ProductsQuery
    {
        NorthwindEntities nwe;

        public ProductsQuery(NorthwindEntities context)
        {
            nwe = context;
        }


        public void getTopProduct()
        {
            getTopProduct(null);
        }

        public void getTopProduct(string countryCode)
        {
            var result = from p in nwe.Products
                         join od in nwe.Order_Details on p.ProductID equals od.ProductID
                         join o in nwe.Orders on od.OrderID equals o.OrderID
                         where (o.OrderDate >= new System.DateTime(1997, 1, 1)
                                && o.OrderDate <= new System.DateTime(1997, 12, 31))
                         select new { p.ProductID, p.ProductName, od.Quantity, o.ShipCountry };

            if (countryCode != null)
            {
                result = result.Where(r => r.ShipCountry == countryCode);
            }


            var result2 = from r in result
                          group r by new { r.ProductID, r.ProductName, } into grp
                          select new
                          {
                              PID = grp.Key.ProductID,
                              PNAME = grp.Key.ProductName,
                              QTY = grp.Sum(r => r.Quantity)
                          };


            var maxQty = result2.OrderByDescending(i => i.QTY)
                .Take(1)
                .Select(i => i.QTY).Single();



            result2.Where(i => i.QTY == maxQty)
            .ToList()
            .ForEach(i => { System.Console.WriteLine(i.PID + " " + i.PNAME + " " + i.QTY); });

        }

    }
}
